import React, { useState, useEffect, useRef } from "react";
import { GnomeCard } from "../../common/gnomeCard/GnomeCard";
import { GnomeDetails } from "../../common/gnomeDetails/GnomeDetails";
import { SearchBar } from "../../common/searchBar/SearchBar";
import { getGnomes } from "../../services/getGnomes";
import { Chip } from "../../common/chip/Chip";

export const Home = () => {
  const [gnomes, setGenomes] = useState([]);
  const [filteredGnomes, setFilteredGnomes] = useState([]);

  const [chipsSelectedProfessions, setChipsSelectedProfessions] = useState([]);
  const [chipsSelectedNames, setChipsSelectedNames] = useState([]);
  const [genomeDetails, setGnomeDetails] = useState([]);
  const [viewDetails, setViewDetails] = useState(false);
  const [totalNumOfItems, setTotalNumOfItems] = useState(11);

  const loader = useRef(null);

  useEffect(() => {
    getGnomes().then((gnomes) => {
      setGenomes(gnomes);
      setFilteredGnomes(gnomes);
    });

    const options = {
      root: null,
      rootMargin: "100px",
      threshold: 1.0,
    };
    const observer = new IntersectionObserver(handleObserver, options);
    if (loader.current) {
      observer.observe(loader.current);
    }
  }, []);

  const handleObserver = (entities) => {
    const target = entities[0];
    if (target.isIntersecting) {
      setTotalNumOfItems((totalNumOfItems) => totalNumOfItems + 11);
    }
  };

  const viewGnomeDetails = (text) => {
    setViewDetails(true);
    const infoDetailGnome = gnomes.filter((id) => id.id === text);
    setGnomeDetails(infoDetailGnome);
  };

  const viewGallery = () => {
    setViewDetails(false);
  };

  const addChipProfession = (chip) => {
    chipsSelectedProfessions.length === 0
      ? setChipsSelectedProfessions([chip.toLocaleLowerCase()])
      : setChipsSelectedProfessions([
          ...chipsSelectedProfessions,
          chip.toLocaleLowerCase(),
        ]);
  };
  const addChipName = (chip) => {
    chipsSelectedNames.length === 0
      ? setChipsSelectedNames([chip.toLocaleLowerCase()])
      : setChipsSelectedNames([
          ...chipsSelectedNames,
          chip.toLocaleLowerCase(),
        ]);
  };

  const upDateChipsListProfessions = () => {
    for (const chip of chipsSelectedProfessions) {
      filterByProfessions(chip);
    }
  };
  useEffect(() => {
    upDateChipsListProfessions();
    setViewDetails(false);
  }, [chipsSelectedProfessions]);

  const filterByProfessions = (chip) => {
    let filterGnomes = [];

    for (const gnome of filteredGnomes) {
      gnome.professions.forEach((element) => {
        if (element.toLocaleLowerCase() === chip) {
          filterGnomes.push(gnome);
        }
      });

      setFilteredGnomes(filterGnomes);
    }
  };

  const upDateChipsListNames = () => {
    for (const chip of chipsSelectedNames) {
      filterByNames(chip);
    }
  };

  useEffect(() => {
    upDateChipsListNames();
    setViewDetails(false);
  }, [chipsSelectedNames]);

  const filterByNames = (chip) => {
    const filterGnomes = filteredGnomes.filter((gnome) =>
      gnome.name.toLocaleLowerCase().includes(chip)
    );
    setFilteredGnomes(filterGnomes);
  };

  const deleteAllFilters = () => {
    setFilteredGnomes(gnomes);
    setChipsSelectedProfessions([]);
    setChipsSelectedNames([]);
  };

  return (
    <>
      <SearchBar
        addChipProfession={addChipProfession}
        addChipName={addChipName}
      />
      <div className="row">
        <div className="col d-flex flex-row justify-content-center flex-wrap">
          {chipsSelectedNames.length || chipsSelectedProfessions.length > 0 ? (
            <Chip
              keyWord={"Delete all filters"}
              chipType={"delete"}
              deleteAllFilters={deleteAllFilters}
            />
          ) : (
            ""
          )}
          {chipsSelectedNames.map((chip, key) => (
            <Chip keyWord={chip} chipType={"name"} key={key} />
          ))}
          {chipsSelectedProfessions.map((chip, key) => (
            <Chip keyWord={chip} chipType={"profession"} key={key} />
          ))}
        </div>
      </div>

      {viewDetails ? (
        <>
          <button
            className="btn btn-light animated fadeIn"
            onClick={viewGallery}
          >
            &#60;&#60; back to gallery
          </button>
          <GnomeDetails {...genomeDetails} />
        </>
      ) : (
        <div className="row row-cols-1 row-cols-sm-3 row-cols-md-4 g-4">
          {filteredGnomes.slice(0, totalNumOfItems).map((gnome, key) => (
            <div className="col" key={key}>
              <GnomeCard {...gnome} viewGnomeDetails={viewGnomeDetails} />
            </div>
          ))}
        </div>
      )}
      <div id="loaderId" ref={loader}></div>
    </>
  );
};
