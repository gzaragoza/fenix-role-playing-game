export const getGnomes = async () => {
  const url =
    "https://bitbucket.org/fenix-group-international/frontend-test/raw/80d1664d5db3a516537a3bbbb4f3fca968d18b2e/data.json";

  const response = await fetch(url);
  const data = await response.json();

  const gnomes = data.Brastlewark.map((gnome) => {
    return {
      id: gnome.id,
      name: gnome.name,
      thumbnail: gnome.thumbnail,
      age: gnome.age,
      weight: gnome.weight,
      height: gnome.height,
      hair_color: gnome.hair_color,
      professions: gnome.professions,
      friends: gnome.friends,
    };
  });

  return gnomes;
};
