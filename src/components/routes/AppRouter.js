import React from "react";
import { Home } from "../pages/home/Home";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Header } from "../common/header/Header";

export const AppRouter = () => {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </div>
    </Router>
  );
};
