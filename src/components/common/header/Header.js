import React from "react";

export const Header = () => {
  return (
    <div className="row text-center mt-4 mb-4 animated fadeIn">
      <div className="col">
        <h1>Brastlewark App</h1>
      </div>
    </div>
  );
};
