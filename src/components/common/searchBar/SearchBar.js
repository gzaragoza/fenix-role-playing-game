import React, { useState } from "react";

export const SearchBar = (props) => {
  const [name, setName] = useState("");
  const [profession, setProfession] = useState("");

  const handleNameChip = (e) => {
    e.target.value !== "" && props.addChipName(e.target.value);
  };
  const handleProfessionChip = (e) => {
    e.target.value !== "" && props.addChipProfession(e.target.value);
  };

  return (
    <>
      <div className="row mb-4">
        <div className="col-12 col-sm-6">
          <input
            id="filter-name"
            type="text"
            className="form-control"
            placeholder="Filter by name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleNameChip(e);
                setName("");
              }
            }}
          />
        </div>
        <div className="col-12 col-sm-6">
          <input
            id="filter-profession"
            type="text"
            className="form-control"
            placeholder="Filter by profession"
            value={profession}
            onChange={(e) => setProfession(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleProfessionChip(e);
                setProfession("");
              }
            }}
          />
        </div>
      </div>
    </>
  );
};
