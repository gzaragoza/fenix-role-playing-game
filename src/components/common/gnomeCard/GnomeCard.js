import React from "react";
import { Chip } from "./../../common/chip/Chip";

export const GnomeCard = (props) => {
  const myFunc = () => {
    props.viewGnomeDetails(props.id);
  };

  return (
    <>
      <div
        className="card shadow h-100 d-flex flex-column align-items-center animated fadeIn"
        onClick={() => {
          myFunc();
        }}
      >
        <div
          className="avatar mt-4"
          style={{ backgroundImage: "url(" + props.thumbnail + ")" }}
        ></div>
        <div className="card-body">
          <h5 className="card-title text-center">{props.name}</h5>

          <span className="card-text d-flex flex-row justify-content-center flex-wrap">
            {props.professions.map((profesion, key) => (
              <Chip keyWord={profesion} chipType={"profession"} key={key} />
            ))}
          </span>
        </div>
      </div>
    </>
  );
};
