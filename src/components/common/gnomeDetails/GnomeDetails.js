import React, { useState } from "react";
import { Chip } from "../../common/chip/Chip";

export const GnomeDetails = (props) => {
  const [gnome, setGnome] = useState(props);

  return (
    <>
      <div className="row  animated fadeIn">
        <div className=" col col-gnome-details-1">
          <div
            className="avatar avatarXl"
            style={{ backgroundImage: "url(" + gnome[0].thumbnail + ")" }}
          ></div>
        </div>
        <div className="col d-flex flex-row align-items-center">
          <ul className="noBullets ">
            <li>
              <h1>{gnome[0].name}</h1>
            </li>
            <li>
              <strong>Age:</strong> {gnome[0].age}
            </li>
            <li>
              <strong>weight:</strong> {gnome[0].weight}
            </li>
            <li>
              <strong>height:</strong> {gnome[0].height}
            </li>
            <li>
              <strong>Hair:</strong> {gnome[0].hair_color}
            </li>
            {gnome[0].professions.length > 0 && (
              <li>
                <strong>Professions:</strong>
                <br />

                {gnome[0].professions.map((profession, key) => (
                  <Chip
                    keyWord={profession}
                    chipType={"profession"}
                    key={key}
                  />
                ))}
              </li>
            )}
            {gnome[0].friends.length > 0 && (
              <li>
                <strong>Friends:</strong>
                <br />
                {gnome[0].friends.map((friend, key) => (
                  <Chip keyWord={friend} chipType={"name"} key={key} />
                ))}
              </li>
            )}
          </ul>
        </div>
      </div>
    </>
  );
};
