import React from "react";

export const Chip = (props) => {
  const deleteFilters = () => {
    props.deleteAllFilters();
  };
  return (
    <>
      {props.chipType === "delete" ? (
        <div
          className="chip animated fadeIn"
          style={{ backgroundColor: "#000", color: "#fff", cursor: "pointer" }}
          onClick={deleteFilters}
        >
          <div className="chipContent">{props.keyWord}</div>
        </div>
      ) : (
        <div
          className="chip animated fadeIn"
          style={
            props.chipType === "profession"
              ? { backgroundColor: "#ddd" }
              : { backgroundColor: "#666", color: "#fff" }
          }
        >
          <div className="chipContent">{props.keyWord}</div>
        </div>
      )}
    </>
  );
};
