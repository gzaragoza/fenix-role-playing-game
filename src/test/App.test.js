import { render } from "@testing-library/react";
import { shallow } from "enzyme";
import { BrastlewarkApp } from "../BrastlewarkApp.js";

const observe = jest.fn();
const wrapper = shallow(<BrastlewarkApp />);

describe("Test in main component BrastlewarkApp", () => {
  test("Snapshot test of BrastlewarkApp", () => {
    expect(wrapper).toMatchSnapshot();
  });

  test("Renders BrastlewarkApp without errors", () => {
    window.IntersectionObserver = jest.fn(function () {
      this.observe = observe;
    });

    render(<BrastlewarkApp />);
  });
});
