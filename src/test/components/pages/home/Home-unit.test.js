import React from "react";
import { shallow } from "enzyme";
import { Home } from "../../../../components/pages/home/Home";

const wrapper = shallow(<Home />);

describe("<Home /> component unit testing", () => {
  test("Snapshot test", () => {
    expect(wrapper).toMatchSnapshot();
  });

  test("Ensure SearchBar component is present into Home component  and has props addChipProfession and addChipName", () => {
    const SearchBar = wrapper.find("SearchBar");
    expect(SearchBar.prop("addChipProfession")).toBeTruthy();
    expect(SearchBar.prop("addChipName")).toBeTruthy();
  });

  test("Ensure GnomeCard and GnomeDetails components are present into Home coponent to display gallery and gnme details correctly", () => {
    const GnomeCard = wrapper.find("GnomeCard");
    const GnomeDetails = wrapper.find("GnomeDetails");
    expect(GnomeCard).toBeTruthy();
    expect(GnomeDetails).toBeTruthy();
  });

  test("Ensure there is a div with ref loader at the end of page to ensure progresive rendering of gnomes gallery is working", () => {
    const DivLoaderDetector = wrapper.find("#loaderId");
    expect(DivLoaderDetector).toBeTruthy();
  });
});
