import { SearchBar } from "../../../../../src/components/common/searchBar/SearchBar";
import { shallow } from "enzyme";

const wrapper = shallow(<SearchBar />);

describe("<Home /> component unit testing", () => {
  test("Snapshot test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
