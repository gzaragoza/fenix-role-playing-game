import { getGnomes } from "../../../components/services/getGnomes";

describe("Unit testing for service getGnomes", () => {
  test("it should fetch at least 1 item ", async () => {
    const gnomesData = await getGnomes();
    expect(gnomesData.length).toBeGreaterThan(0);
  });
});
