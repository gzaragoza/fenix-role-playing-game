import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import { BrastlewarkApp } from "./BrastlewarkApp";

ReactDOM.render(<BrastlewarkApp />, document.getElementById("root"));
