import React from "react";
import { Header } from "./components/common/header/Header";
import { Home } from "./components/pages/home/Home";

export const BrastlewarkApp = () => {
  return (
    <div className="container-fluid pl-4 pr-5">
      <Header />
      <Home />
    </div>
  );
};
